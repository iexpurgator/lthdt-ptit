/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package java_env.ex2;

import java.math.BigInteger;
import java.util.Scanner;

/**
 *
 * @author Hoan
 */
public class Ex2 {

    public static void main(String[] args) {
        proc();
    }

    private static void proc() {
        CapSoNguyen csn = new CapSoNguyen();
        csn.Nhap();
        csn.LCM_GCD();
        csn.primeInRangeNM();
        csn.reversibleInRangeNM();
    }
}

class CapSoNguyen {

    private BigInteger n;
    private BigInteger m;

    public CapSoNguyen() {
    }

    public CapSoNguyen(BigInteger n) {
        this.n = n;
    }

    public void Nhap(){
        Scanner in = new Scanner(System.in);
        System.out.print("Nhap n: ");
        this.n = in.nextBigInteger();
        System.out.print("Nhap m: ");
        this.m = in.nextBigInteger();
        if (n.compareTo(m) > 0) {
            BigInteger tmp = m;
            m = n;
            n = tmp;
        }
    }
    
    public void LCM_GCD(){
        System.out.print("Uoc chung lon nhat cua " + n + " va " + m + " la: ");
        System.out.println(n.gcd(m));
        System.out.print("Boi chung nho nhat cua " + n + " va " + m + " la: ");
        System.out.println(n.multiply(m).divide(n.gcd(m)));
    }
    
    public void primeInRangeNM(){
        System.out.println("So nguyen to tu " + n + " den " + m + " la: ");
        BigInteger sm = n;
        BigInteger bi = m;
        while(sm.compareTo(bi) < 0) {
            if (sm.isProbablePrime(1)){
                System.out.print(sm + " ");
                sm = sm.nextProbablePrime();
            } else {
                sm = sm.nextProbablePrime();
            }
        }
        System.out.println("");
    }
    
    public void reversibleInRangeNM(){
        System.out.println("So thuan nghich tu " + n + " den " + m + " la: ");
        BigInteger reversible = n;
        BigInteger endcheck = m;
        while(reversible.compareTo(endcheck) < 0) {
            String s = reversible.toString();
            byte[] strAsByteArray = s.getBytes();
            byte[] result = new byte[strAsByteArray.length];
            for (int i = 0; i < strAsByteArray.length; i++)
                result[i] = strAsByteArray[strAsByteArray.length - i - 1];
            String s_rev = new String(result);
            if (s.equals(s_rev)) {
                System.out.print(s + " ");
            }
            reversible = reversible.add(BigInteger.ONE);
        }
        System.out.println("");
    }
}
