/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package java_env.ex1;

import java.math.BigInteger;
import java.util.Arrays;
import java.util.Scanner;

/**
 *
 * @author Hoan
 */
public class Ex1 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        proc();
    }

    public static void proc() {
        SoNguyen n = new SoNguyen(BigInteger.ZERO);
        Boolean exitloop = false;
        do {
            int option = menu();
            switch (option) {
                case 1:
                    System.out.print("n = ");
                    n.Nhap();
                    break;
                case 2:
                    n.TongTu1DenN();
                    break;
                case 3:
                    n.factorial();
                    break;
                case 4:
                    n.TongSoChanTu1DenN();
                    break;
                case 5:
                    n.TongSoLeTu1DenN();
                    break;
                case 6:
                    n.SoNguyenToNhoHonN();
                    break;
                case 7:
                    n.getFib();
                    break;
                case 8:
                    n.sumDigits();
                    break;
                case 9:
                    n.printDivisors();
                    break;
                default:
                    System.out.println("Not option!!!");
                    exitloop = true;
                    break;
            }
        } while(!exitloop);
    }

    public static int menu() {
        System.out.println("\tMenu");
        System.out.println("1. Nhap so nguyen duong n");
        System.out.println("2. Tong 1+2+3+...+n");
        System.out.println("3. Tich 1*2*3*...*n");
        System.out.println("4. Tong 2+4+6+...");
        System.out.println("5. Tong 1+3+5+...");
        System.out.println("6. So nguyen to <n");
        System.out.println("7. So fibonacci <n");
        System.out.println("8. Tong cac chu so cua n");
        System.out.println("9. Cac uoc cua n");
        Scanner in = new Scanner(System.in);
        System.out.print("\nEnter number: ");
        int choose = in.nextInt();
        return choose;
    }
}

class SoNguyen {

    private BigInteger n;

    public SoNguyen() {
    }

    public SoNguyen(BigInteger n) {
        this.n = n;
    }

    public void Nhap() {
        Scanner input = new Scanner(System.in);
        this.n = input.nextBigInteger();
    }

    public void TongTu1DenN() {
        BigInteger sum = n.multiply(n.add(BigInteger.ONE)); // sum = n * (n + 1)
        sum = sum.divide(BigInteger.valueOf(2)); // sum = sum / 2
        System.out.println(sum);
    }

    public void factorial() { // tich cac so tu 1 den n
        BigInteger num = new BigInteger("1");
        BigInteger i = n;
        while (i.compareTo(BigInteger.ONE) != 0) {
            num = num.multiply(i);
            i = i.subtract(BigInteger.ONE);
        }
        System.out.println(num);
    }

    public void TongSoLeTu1DenN() {
        BigInteger sumOdd = n.add(BigInteger.ONE); // sumOdd = n+1
        sumOdd = sumOdd.divide(BigInteger.valueOf(2)); // sumOdd = sumOdd / 2
        sumOdd = sumOdd.multiply(sumOdd); // sumOdd =  sumOdd * sumOdd
        System.out.println(sumOdd);
    }

    public void TongSoChanTu1DenN() {
        BigInteger sumEven = n.divide(BigInteger.valueOf(2)); // sumEven = n/2
        sumEven = sumEven.multiply(sumEven.add(BigInteger.ONE)); // sumEven =  sumEven * (sumEven + 1)
        System.out.println(sumEven);
    }

    public void sieveOfEratosthenes() {
        boolean[] prime = new boolean[n.intValue() + 1];
        Arrays.fill(prime, true);
        prime[0] = prime[1] = false;
        for (int p = 2; p * p < n.intValue() + 1; ++p) {
            if (prime[p] == true) {
                for (int i = p * p; i < n.intValue() + 1; i += p) {
                    prime[i] = false;
                }
            }
        }
        for (int p = 2; p < n.intValue(); p++) {
            if (prime[p]) {
                System.out.print(p + " ");
            }
        }
        System.out.println("");
    }

    public void SoNguyenToNhoHonN() {
        BigInteger prime = new BigInteger("2");
        while (prime.compareTo(n) < 0) {
            System.out.print(prime + " ");
            prime = prime.nextProbablePrime();
        }
        System.out.println("");
    }

    public void getFib() {
        BigInteger a = new BigInteger("1");
        BigInteger b = new BigInteger("1");
        while (true) {
            if (n.compareTo(a.add(b)) < 0) {
                break;
            }
            System.out.print(a + " ");
            BigInteger c = a.add(b);
            a = b;
            b = c;
        }
        System.out.println("");
    }

    public void sumDigits() {
        String digits = n.toString();
        BigInteger sum = BigInteger.ZERO;
        for (char c : digits.toCharArray()) {
            sum = sum.add(BigInteger.valueOf(c - '0'));
        }
        System.out.println(sum);
    }

    private BigInteger sqrt(BigInteger x) {
        BigInteger div = BigInteger.ZERO.setBit(x.bitLength() / 2);
        BigInteger div2 = div;
        // Loop until we hit the same value twice in a row, or wind
        // up alternating.
        for (;;) {
            BigInteger y = div.add(x.divide(div)).shiftRight(1);
            if (y.equals(div) || y.equals(div2)) {
                return y;
            }
            div2 = div;
            div = y;
        }
    }

    public void printDivisors() {
        for (BigInteger i = BigInteger.ONE;
                i.multiply(i).compareTo(n) < 0;
                i = i.add(BigInteger.ONE)) {
            if (n.mod(i).compareTo(BigInteger.ZERO) == 0) {
                System.out.print(i + " ");
            }
        }
        for (BigInteger i = sqrt(n);
                i.compareTo(BigInteger.ONE) >= 0;
                i = i.subtract(BigInteger.ONE)) {
            if (n.mod(i).compareTo(BigInteger.ZERO) == 0) {
                System.out.print(n.divide(i) + " ");
            }
        }
        System.out.println("");
    }
}
