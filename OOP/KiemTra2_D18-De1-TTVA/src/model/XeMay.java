package model;

import java.util.Calendar;

/**
 *
 * @author Hoan
 */
public class XeMay extends PTGT {

    private double congXuat;

    public XeMay() {
    }

    public XeMay(String soKhung, int nam, double gia, String mau, double congXuat) {
        super(soKhung, nam, gia, mau);
        this.congXuat = congXuat;
    }

    public double isCongXuat() {
        return congXuat;
    }

    public void setCongXuat(double congXuat) {
        this.congXuat = congXuat;
    }

    @Override
    public double getGia() {
        Calendar c = Calendar.getInstance();
        int y = c.get(Calendar.YEAR);
        if (super.getNam() >= (y - 2)) {
            return super.getGia();
        } else {
            return super.getGia() * 0.9;
        }
    }

    @Override
    public String toString() {
        return String.format("%12s %8d %8.1f %15s %15.1f", getSoKhung(), getNam(), getGia(), getMau(), congXuat);
    }
}
