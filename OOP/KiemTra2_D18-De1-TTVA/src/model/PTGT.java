package model;

/**
 *
 * @author Hoan
 */
//public class PTGT implements Comparable<PTGT> { /* override compareTo*/
public class PTGT {

    private String soKhung;
    private int nam;
    private double gia;
    private String mau;

    /*
    so khung xe - length 10 -number & char
    nam sx
    gia ban
    mau
     */
    public PTGT() {
    }

    public PTGT(String soKhung, int nam, double gia, String mau) {
        this.soKhung = soKhung;
        this.nam = nam;
        this.gia = gia;
        this.mau = mau;
    }

    public String getSoKhung() {
        return soKhung;
    }

    public void setSoKhung(String soKhung) {
        this.soKhung = soKhung;
    }

    public int getNam() {
        return nam;
    }

    public void setNam(int nam) {
        this.nam = nam;
    }

    public double getGia() {
        return gia;
    }

    public void setGia(double gia) {
        this.gia = gia;
    }

    public String getMau() {
        return mau;
    }

    public void setMau(String mau) {
        this.mau = mau;
    }

    /**
     *
     * @return
     */
    @Override
    public String toString() {
        return String.format("%12s %8d %10.4f %20s", soKhung, nam, gia, mau);
    }

//    @Override
//    public int compareTo(PTGT o) {
//        /*
//        xap xep theo nam
//         */
////        return this.getNam() - o.getNam();
//        /*
//        xap xep theo gia
//         */
//        if (getGia() > o.getGia()) {
//            return 1;
//        } else if (getGia() == o.getGia()) {
//            return 0;
//        } else {
//            return -1;
//        }
//    }
}
