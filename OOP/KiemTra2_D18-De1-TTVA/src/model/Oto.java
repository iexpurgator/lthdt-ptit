package model;

import java.util.Calendar;

/**
 *
 * @author Hoan
 */
public class Oto extends PTGT{
    private int soCho;

    public Oto() {
    }

    public Oto(String soKhung, int nam, double gia, String mau, int soCho) {
        super(soKhung, nam, gia, mau);
        this.soCho = soCho;
    }

    public int getSoCho() {
        return soCho;
    }

    public void setSoCho(int soCho) {
        this.soCho = soCho;
    }
    
    @Override
    public double getGia(){
        Calendar c = Calendar.getInstance();
        int y = c.get(Calendar.YEAR);
        if (super.getNam() >= (y - 2)){
            return super.getGia();
        } else
            return super.getGia()*0.7;
    }
    
    @Override
    public String toString(){
        return String.format("%12s %8d %8.1f %15s %15d", getSoKhung(), getNam(), getGia(), getMau(), soCho);
    }
}
