package controller;

/**
 *
 * @author Hoan
 */
public interface ChucNang {
    public void nhapOto();
    public void nhapXeMay();
    public void viet();
    public void sua(String soKhung);
    public void sapXepSoKhung();
    public void vietPTDatDotheoMau();
}
