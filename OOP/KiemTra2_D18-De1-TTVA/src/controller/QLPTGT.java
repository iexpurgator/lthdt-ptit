package controller;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Scanner;
import java.util.Set;
import java.util.stream.Collectors;
import model.Oto;
import model.PTGT;
import model.XeMay;

/**
 *
 * @author Hoan
 */
public class QLPTGT implements ChucNang {

    private List<PTGT> list;
    private Scanner sc;

    public QLPTGT() {
        list = new ArrayList<>();
        sc = new Scanner(System.in);
        list.add(new Oto("HGF1234567", 2021, 1000, "do", 4));
        list.add(new Oto("KCA2485098", 2012, 600, "xanh", 4));
        list.add(new Oto("BBF1182938", 2008, 800, "vang", 4));
        list.add(new Oto("ABC1992999", 2008, 800, "den", 4));
        list.add(new XeMay("EXM2740598", 2018, 40, "trang", 28));
        list.add(new XeMay("DMM2098470", 2016, 30, "den", 21));
    }

    private int TonTai(String soKhung) {
        for (int i = 0; i < list.size(); i++) {
            if (list.get(i).getSoKhung().equalsIgnoreCase(soKhung)) {
                return i;
            }
        }
        return -1;
    }

    private PTGT nhap() {
        String soKhung = "";
        while (true) {
            System.out.print("Nhap so khung: ");
            String re = "[A-Z0-9]{10}";
            soKhung = sc.nextLine().toUpperCase();
            if ((TonTai(soKhung) == -1) && soKhung.matches(re)) {
                break;
            } else {
                System.err.println("Nhap lai!!!");
            }
        }
        int nam;
        System.out.print("Nhap nam (so): ");
        nam = Integer.parseInt(sc.nextLine());
        double gia;
        System.out.print("Nhap gia (so): ");
        gia = Integer.parseInt(sc.nextLine());
        String mau;
        System.out.print("Nhap mau: ");
        mau = sc.nextLine();
        return new PTGT(soKhung, nam, gia, mau);
    }

    @Override
    public void nhapOto() {
        PTGT p = nhap();
        int soCho;
        System.out.print("Nhap so cho (so): ");
        soCho = Integer.parseInt(sc.nextLine());
        list.add(new Oto(p.getSoKhung(), p.getNam(), p.getGia(), p.getMau(), soCho));
    }

    @Override
    public void nhapXeMay() {
        PTGT p = nhap();
        double congSuat;
        System.out.print("Nhap cong suat (so): ");
        congSuat = Integer.parseInt(sc.nextLine());
        list.add(new XeMay(p.getSoKhung(), p.getNam(), p.getGia(), p.getMau(), congSuat));
    }

    @Override
    public void viet() {
        System.out.println(String.format("%12s %8s %8s %15s %15s", "SoKhung", "Nam", "Gia", "Mau", "Cho or CSuat"));
        list.forEach(System.out::println);
    }

    @Override
    public void sua(String soKhung) {
        int vitri = TonTai(soKhung);
        if (vitri == -1) {
            System.err.println("Khong co!!!");
        } else {
            PTGT p;
            p = list.get(vitri);
            int nam;
            System.out.print("Nhap nam (so): ");
            nam = Integer.parseInt(sc.nextLine());
            p.setNam(nam);
            double gia;
            System.out.print("Nhap gia (so): ");
            gia = Integer.parseInt(sc.nextLine());
            p.setGia(gia);
            String mau;
            System.out.print("Nhap mau: ");
            mau = sc.nextLine();
            p.setMau(mau);
            System.out.println("Sua thanh cong!!!");
        }
    }

    public void xoa(String soKhung) {
        int vitri = TonTai(soKhung);
        if (vitri == -1) {
            System.err.println("Khong tim thay de xoa!!!");
        } else {
            list.remove(vitri);
            System.out.println("Xoa thanh cong!!!");
        }
    }

    @Override
    public void sapXepSoKhung() {
        viet();
//        Collections.sort(list); /*using with override compareTo*/
        Collections.sort(list, new Comparator<PTGT>() {
            @Override
            public int compare(PTGT t, PTGT t1) {
//                return t.getSoKhung().compareToIgnoreCase(t1.getSoKhung());
                // 2 thuoc tinh: theo mau, neu mau giong nhau thi giam dan theo gia
                if (t.getMau().equalsIgnoreCase(t1.getMau())) {
                    return (int) (t1.getGia() - t.getGia());
                } else {
                    return t.getMau().compareToIgnoreCase(t1.getMau());
                }
            }
            /**
             * mot so meo khi dung compare
             *
             * vd: voi ho va ten tieng viet khi viet thong thuong se sort theo
             * ho, muon doi lai thanh xap xep theo then co the su dung
             *
             * String name1 = "tran quoc hoan"; 
             * \\ split nhieu dau cach 
             * String[] s = name1.split("\\s+");
             * String nn = [s.legth -1] + name1; \\ hoantran quoc hoan
             *
             * vd: voi ngay thang nam
             *
             * String d = "12/07/2000";
             * String[] s = d.split("/"); 
             * String dd = s[2] + "/" + s[1] + "/" + s[0];
             *
             * vd: sort 2 thuoc tinh (nhu tren)
             *
             *
             *
             */
        });
        viet();
    }

    @Override
    public void vietPTDatDotheoMau() {
        // thong ke: sum, avg , count, max, min
//        // dem theo mau
//        Map<String, Long> count;
//        count = list.stream().collect(Collectors.groupingBy(PTGT::getMau, Collectors.counting()));
//        for(Object key:count.keySet())
//            System.out.println("Mau "+ key.toString() + ": so luong "+ count.get(key));
//        // tong so tien theo nam
//        Map<Integer, Double> sum;
//        sum = list.stream().collect(Collectors.groupingBy(PTGT::getNam, Collectors.summingDouble(PTGT::getGia)));
//        for(Object key:sum.keySet())
//            System.out.println("Nam "+ key.toString() + ": tong tien "+ sum.get(key));
//        // viet ra xe co gia cao nhat = max
//        Optional<PTGT> max = list.stream().collect(Collectors.maxBy(Comparator.comparing(PTGT::getGia)));
//        System.out.println("PTGT co gia lon nhat:\n" + (max.isPresent()?max.get():"khong co"));
        // phantu dat nhat theo mau
        Map<String, PTGT> o = list.stream().collect(
                Collectors.groupingBy(
                        (PTGT::getMau),
                        Collectors.collectingAndThen(
                                Collectors.reducing(((PTGT p1, PTGT p2) -> p1.getGia() > p2.getGia() ? p1 : p2)),
                                Optional::get
                        )
                )
        );
        for(String key:o.keySet())
            System.out.println("Mau "+ key + ":\n\tGia cao nhat "+ o.get(key));
    }
}
