package view;

import controller.QLPTGT;
import java.util.Scanner;

/**
 *
 * @author Hoan
 */
public class Main {
    private static Scanner sc = new Scanner(System.in);
    public static void main(String[] args) {
        QLPTGT q = new QLPTGT();
        int choose;
        do {
            System.out.println("1. Nhap oto");
            System.out.println("2. Nhap xe may");
            System.out.println("3. Viet");
            System.out.println("4. Sua");
            System.out.println("5. Xoa");
            System.out.println("6. Xap xep so khung");
            System.out.println("7. Thong Ke");
            System.out.println("0. Thoat");
            System.out.print("Nhap: ");
            choose = Integer.parseInt(sc.nextLine());
            switch(choose){
                case 1:
                    q.nhapOto();
                    break;
                case 2:
                    q.nhapXeMay();
                    break;
                case 3:
                    q.viet();
                    break;
                case 4:
                    String soKhungs;
                    System.out.print("Nhap so khung: ");
                    soKhungs = sc.nextLine();
                    q.sua(soKhungs);
                    break;
                case 5:
                    String soKhungx;
                    System.out.print("Nhap so khung: ");
                    soKhungx = sc.nextLine();
                    q.xoa(soKhungx);
                    break;
                case 6:
                    q.sapXepSoKhung();
                    break;
                case 7:
                    q.vietPTDatDotheoMau();
                    break;
                case 0:
                    System.out.println("Bye!!!");
                    break;
                default:
                    System.out.println("Nhap sai!!!\nChi nhap (0-7)");
                    break;
            }
        } while(choose != 0);
    }
}
