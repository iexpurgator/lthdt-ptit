package oop.q1;

import java.util.ArrayList;
import java.util.Map;
import java.util.stream.Collectors;
import static oop.q1.OOPQ1.scan;

/**
 *
 * @author Hoan
 */
class PhoneBook {

    private final ArrayList<PhoneNumber> BookPhones;

    public PhoneBook() {
        this.BookPhones = new ArrayList<>();
        BookPhones.add(new PhoneNumber("024", "36407515"));
        BookPhones.add(new IntlPhoneNumber("84", "216", "3821213"));
        BookPhones.add(new IntlPhoneNumber("1", "933", "28734509"));
        BookPhones.add(new PhoneNumber("036", "43807632"));
        BookPhones.add(new IntlPhoneNumber("84", "24", "1324087"));
    }

    public void nhapPhoneNum() {
        String PhoneNum;
        while (true) {
            System.out.print("Nhap so dien thoai: ");
            PhoneNum = scan.nextLine();
            // ma vung 3-4 so bat dau bang 0 "-" nhieu so
            if (PhoneNum.matches("^[0]{1}\\d{2,3}-\\d+")) {
                String[] Num = PhoneNum.split("-");
                BookPhones.add(new PhoneNumber(Num[0], Num[1]));
                break;
            } else {
                System.err.println("So dien thoai trong nuoc co dang "
                        + "024-36407515 hoac 0227-3821213");
            }
        }
    }

    public void nhapIntlPhoneNum() {
        String PhoneNum;
        while (true) {
            System.out.print("Nhap so dien thoai: ");
            PhoneNum = scan.nextLine();
            // ma qg 1-3 so "-" ma vung 2-3 so bat dau khac 0 "-" nhieu so
            if (PhoneNum.matches("^\\d{1,3}-[1-9]{1}\\d{1,2}-\\d+")) {
                String[] Num = PhoneNum.split("-");
                BookPhones.add(new IntlPhoneNumber(Num[0], Num[1], Num[2]));
                break;
            } else {
                System.err.println("So dien thoai quoc te co dang "
                        + "84-24-36407515 hoac 84-227-3821213");
            }
        }
    }

    public void searchMaVung() {
        String MaVung;
        System.out.print("Nhap ma vung: ");
        MaVung = scan.nextLine();
        BookPhones.stream().filter((i) -> (i.getMaVung().equalsIgnoreCase(MaVung) || i.getMaVung().equalsIgnoreCase("0" + MaVung))).forEachOrdered(System.out::println);
        System.out.println();
    }

    public void searchSDTNoiVung() {
        String subSDT;
        System.out.print("Nhap so trong phan So dien thoai noi vung can tim kiem: ");
        subSDT = scan.nextLine();
        BookPhones.stream().filter((i) -> (i.getSDTNoiVung().contains(subSDT))).forEachOrdered(System.out::println);
        System.out.println();
    }

    public void total() {
        Map<String, Long> count;
        count = BookPhones.stream().collect(Collectors.groupingBy(PhoneNumber::getMaVung, Collectors.counting()));
//        for (String key : count.keySet()) {
//            System.out.println("Ma vung " + key + " co " + count.get(key) + " SDT: ");
//            BookPhones.stream().filter((i) -> (i.getMaVung().equalsIgnoreCase(key))).forEachOrdered((i) -> {
//                System.out.println("\t" + i);
//            });
//        }
        count.keySet().stream().map((key) -> {
            System.out.println("Ma vung " + key + " co " + count.get(key) + " SDT: ");
            return key;
        }).forEachOrdered((key) -> {
            BookPhones.stream().filter((i) -> (i.getMaVung().equalsIgnoreCase(key))).forEachOrdered((i) -> {
                System.out.println("\t" + i);
            });
        });
        System.out.println();
    }

    public ArrayList<PhoneNumber> getBookPhones() {
        return BookPhones;
    }
}
