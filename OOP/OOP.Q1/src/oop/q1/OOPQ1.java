package oop.q1;

import java.util.Scanner;

/**
 *
 * @author Hoan
 */
public class OOPQ1 {

    /**
     * @param args the command line arguments
     */
    // 84-24-36407515 0227-3821213 84-227-3821213 024-36407515
    static final Scanner scan = new Scanner(System.in);

    public static void main(String[] args) {
        PhoneBook hoan = new PhoneBook();
        int choose;
        do {
            System.out.println("1. Them so dien thoai");
            System.out.println("2. Tim so dien thoai theo vung");
            System.out.println("3. Liet ke so dien thoai");
            System.out.println("4. Tong so dien thoai theo vung");
            System.out.println("5. Tim kiem so dien thoai");
            System.out.println("0. Thoat");
            System.out.println("\nNhap lua chon: ");
            choose = Integer.parseInt(scan.nextLine());
            switch (choose) {
                case 1:
                    Them(hoan);
                    break;
                case 2:
                    hoan.searchMaVung();
                    break;
                case 3:
                    LietKe(hoan);
                    break;
                case 4:
                    hoan.total();
                    break;
                case 5:
                    hoan.searchSDTNoiVung();
                    break;
                case 0:
                    System.out.println("Bye!!!");
                    break;
                default:
                    System.err.println("Nhap sai!!!");
                    break;
            }
        } while (choose != 0);
    }

    static void Them(PhoneBook cur) {
        int choose;
        do {
            System.out.println("");
            System.out.println("1. Them so dien thoai trong nuoc");
            System.out.println("2. Them so dien thoai quoc te");
            System.out.println("0. Thoat");
            System.out.println("\nNhap lua chon: ");
            choose = Integer.parseInt(scan.nextLine());
            switch (choose) {
                case 1:
                    cur.nhapPhoneNum();
                    break;
                case 2:
                    cur.nhapIntlPhoneNum();
                    break;
                case 0:
                    System.out.println("\nMenu:");
                    break;
                default:
                    System.err.println("Nhap sai!!!");
                    break;
            }
        } while (choose != 0);

    }

    static void LietKe(PhoneBook cur) {
        int choose;
        do {
            System.out.println("");
            System.out.println("1. Danh sach so dien thoai trong nuoc");
            System.out.println("2. Danh sach so dien thoai quoc te");
            System.out.println("3. Danh sach tat ca so dien thoai");
            System.out.println("0. Thoat");
            System.out.println("\nNhap lua chon: ");
            choose = Integer.parseUnsignedInt(scan.nextLine());
            switch (choose) {
                case 1:
                    if (!cur.getBookPhones().isEmpty()) {
                        cur.getBookPhones().stream().filter((i) -> (!(i instanceof IntlPhoneNumber))).forEachOrdered(System.out::println);
                    } else {
                        System.err.println("Khong co du lieu!!!");
                    }
                    break;
                case 2:
                    if (!cur.getBookPhones().isEmpty()) {
                        cur.getBookPhones().stream().filter((i) -> (i instanceof IntlPhoneNumber)).forEachOrdered(System.out::println);
                    } else {
                        System.err.println("Khong co du lieu!!!");
                    }
                    break;
                case 3:
                    if (!cur.getBookPhones().isEmpty()) {
                        cur.getBookPhones().forEach(System.out::println);
                    } else {
                        System.err.println("Khong co du lieu!!!");
                    }
                    break;
                case 0:
                    System.out.println("\nMenu:");
                    break;
                default:
                    System.err.println("Nhap sai!!!");
                    break;
            }
        } while (choose != 0);
    }

}
