package oop.q1;

/**
 *
 * @author Hoan
 */
public class PhoneNumber {

    private String MaVung;
    private String SDTNoiVung;

    public PhoneNumber() {
    }

    public PhoneNumber(String MaVung, String SDTNoiVung) {
        if (check(MaVung)) {
            this.MaVung = MaVung;
            this.SDTNoiVung = SDTNoiVung;
        }
    }

    public void setMaVung(String MaVung) {
        if (MaVung != null && check(MaVung)) {
            this.MaVung = MaVung.substring(1);
        }
    }

    public void setSDTNoiVung(String SDTNoiVung) {
        this.SDTNoiVung = SDTNoiVung;
    }

    public String getMaVung() {
        return MaVung;
    }

    public String getSDTNoiVung() {
        return SDTNoiVung;
    }

    private boolean check(String MaVung) {
        if (MaVung.length() > 4 || MaVung.length() < 2 || MaVung.charAt(0) != '0') {
            System.err.println("Khong dung dinh dang so dien thoai!");
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return MaVung + "-" + SDTNoiVung;
    }
}
