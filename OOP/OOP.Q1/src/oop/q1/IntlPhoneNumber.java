package oop.q1;

/**
 *
 * @author Hoan
 */
public class IntlPhoneNumber extends PhoneNumber {

    private String MaQuocGia;

    public IntlPhoneNumber() {
    }

    public IntlPhoneNumber(String MaQuocGia, String MaVung, String SDTNoiVung) {
        super("0" + MaVung, SDTNoiVung);
        this.MaQuocGia = MaQuocGia;
    }

    @Override
    public void setMaVung(String MaVung) {
        super.setMaVung("0" + MaVung);
    }

    public void setMaQuocGia(String MaQuocGia) {
        this.MaQuocGia = MaQuocGia;
    }

    public String getMaQuocGia() {
        return this.MaQuocGia;
    }

    @Override
    public String toString() {
        return MaQuocGia + "-" + getMaVung().substring(1) + "-" + getSDTNoiVung();
    }
}
