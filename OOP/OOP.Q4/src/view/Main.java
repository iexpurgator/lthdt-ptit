package view;

import control.QLGV;
import java.util.Scanner;

/**
 *
 * @author Hoan
 */
public class Main {

    private static Scanner in = new Scanner(System.in);

    public static void main(String[] args) {
        int choose;
        QLGV ds = new QLGV();
        do {
            System.out.println("1. Nhap giang vien co huu");
            System.out.println("2. Nhap giang vien thing giang");
            System.out.println("3. ");
            System.out.println("4. Xoa giang vien");
            System.out.println("5. Sua giang vien");
            System.out.println("6. ");
            System.out.println("7. ");
            System.out.println("8. Sap xep");
            System.out.println("9. ");
            System.out.println("10. ");
            System.out.println("0. Thoat");
            System.out.print("Nhap: ");
            choose = Integer.parseInt(in.nextLine());
            switch (choose) {
                case 1:
                    ds.nhapGVCoHuu();
                    break;
                case 2:
                    ds.nhapGVThGiang();
                    break;
                case 3:
                    ds.dsGV();
                    break;
                case 4:
                    String maGV1;
                    maGV1 = in.nextLine();
                    ds.xoa(maGV1);
                    break;
                case 5:
                    String maGV2;
                    maGV2 = in.nextLine();
                    ds.sua(maGV2);
                    break;
                case 6:
                    ds.dsGVch();
                    break;
                case 7:
                    ds.dsGVtg();
                    break;
                case 8:
                    ds.sapxep();
                    break;
                case 9:
                    ds.tinhluong();
                    break;
                case 10:
                    break;
                case 0:
                    System.out.println("Bye");
                    break;
                default:
                    System.err.println("Nhap lai tu 0-10");
                    break;
            }
        } while (choose != 0);
    }
}
