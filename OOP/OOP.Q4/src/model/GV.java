package model;

/**
 *
 * @author Hoan
 */
public class GV implements TinhLuong{

    /*
    Thông tin giảng viên cơ hữu: mã giảng viên,tên giảng viên, email, học hàm (không, giáo sư, phó
    giáo sư), học vị (đại học, thạc sĩ, tiến sĩ), địa chỉ, điện thoại, số giờ giảng dạy trong tháng, lương
    thỏa thuận (lương cứng ), hệ số lương (đại học: 1, thạc sĩ 1.1 tiến sĩ 1.2, giáo sư thêm 0.2, phó
    giáo sư 0.1) và số giờ quy định chung trong tháng, vượt giờ được tính 50.000đ/1h.
    
    Thông tin giảng viên thỉnh giảng: mã giảng viên , tên giảng viên, email, học hàm (không, giáo
    sư, phó giáo sư), học vị (đại học, thạc sĩ, tiến sĩ), địa chỉ, điện thoại, số giờ giảng dạy trong tháng,
    hệ số lương (đại học: 1, thạc sĩ 1.1 tiến sĩ 1.2, giáo sư thêm 0.2, phó giáo sư 0.1),địa chỉ, điện thoại,
    cơ quan làm việc. Biết rằng mỗi giờ dạy có giá 200.000đ.
     */
    private String maGV;
    private String ten;
    private String email;
    private String hocVi;
    private String hocHam;
    private String diaChi;
    private String dienThoai;
    private int soGioDay;
    private double heSoLuong = 0.9;

    public GV() {
    }

    public GV(String maGV, String ten, String email,String hocVi, String hocHam,  String diaChi, String dienThoai, int soGioDay) {
        this.maGV = maGV;
        this.ten = ten;
        this.email = email;
        this.hocHam = hocHam;
        this.hocVi = hocVi;
        this.diaChi = diaChi;
        this.dienThoai = dienThoai;
        this.soGioDay = soGioDay;
    }

    public String getMaGV() {
        return maGV;
    }

    public void setMaGV(String maGV) {
        this.maGV = maGV;
    }

    public String getTen() {
        return ten;
    }

    public void setTen(String ten) {
        this.ten = ten;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getHocHam() {
        return hocHam;
    }

    public void setHocHam(String hocHam) {
        this.hocHam = hocHam;
    }

    public String getHocVi() {
        return hocVi;
    }

    public void setHocVi(String hocVi) {
        this.hocVi = hocVi;
    }

    public String getDiaChi() {
        return diaChi;
    }

    public void setDiaChi(String diaChi) {
        this.diaChi = diaChi;
    }

    public String getDienThoai() {
        return dienThoai;
    }

    public void setDienThoai(String dienThoai) {
        this.dienThoai = dienThoai;
    }

    public int getSoGioDay() {
        return soGioDay;
    }

    public void setSoGioDay(int soGioDay) {
        this.soGioDay = soGioDay;
    }

    public double getHeSoLuong() {
        switch(getHocVi()){
            case "DH":
                heSoLuong = 1;
                break;
            case "ThS":
                heSoLuong = 1.1;
                break;
            case "TS":
                heSoLuong = 1.2;
                break;
            default:
                break;
        }
        switch(getHocHam()){
            case "PGS":
                heSoLuong += 0.1;
                break;
            case "GS":
                heSoLuong += 0.2;
                break;
            default:
                break;
        }
        return heSoLuong;
    }

    @Override
    public double getLuong() {
        return getHeSoLuong() * getSoGioDay();
    }
    
    
}
