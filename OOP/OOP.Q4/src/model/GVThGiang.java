package model;

/**
 *
 * @author Hoan
 */
public class GVThGiang extends GV implements TinhLuong {

    /*
    Thông tin giảng viên thỉnh giảng: mã giảng viên , tên giảng viên, email, học hàm (không, giáo
    sư, phó giáo sư), học vị (đại học, thạc sĩ, tiến sĩ), địa chỉ, điện thoại, số giờ giảng dạy trong tháng,
    hệ số lương (đại học: 1, thạc sĩ 1.1 tiến sĩ 1.2, giáo sư thêm 0.2, phó giáo sư 0.1),địa chỉ, điện thoại,
    cơ quan làm việc. Biết rằng mỗi giờ dạy có giá 200.000đ.
     */

    private String dcCoQuan;
    private String sdtCoQuan;

    public GVThGiang(String maGV, String ten, String email, String hocVi, String hocHam, String diaChi, String dienThoai, int soGioDay, String dcCoQuan, String sdtCoQuan) {
        super(maGV, ten, email, hocVi, hocHam, diaChi, dienThoai, soGioDay);
        this.dcCoQuan = dcCoQuan;
        this.sdtCoQuan = sdtCoQuan;
    }

    public String getDcCoQuan() {
        return dcCoQuan;
    }

    public void setDcCoQuan(String dcCoQuan) {
        this.dcCoQuan = dcCoQuan;
    }

    public String getSdtCoQuan() {
        return sdtCoQuan;
    }

    public void setSdtCoQuan(String sdtCoQuan) {
        this.sdtCoQuan = sdtCoQuan;
    }

    @Override
    public double getLuong() {
        return getSoGioDay() * 200000 * getHeSoLuong();
    }

    @Override
    public String toString() {
        return String.format("%5s %20s %12s %4s %4s %12s %11s %3s %3.1f %12.0f %11s %12s", // 108
                getMaGV(), getTen(), getEmail(), getHocVi(), getHocHam(), getDiaChi(), getDienThoai(), getSoGioDay(), getHeSoLuong(), getLuong(), dcCoQuan, sdtCoQuan);
    }
}
