package model;

/**
 *
 * @author Hoan
 */
public class GVCoHuu extends GV implements TinhLuong {

    /*
    Thông tin giảng viên cơ hữu: mã giảng viên,tên giảng viên, email, học hàm (không, giáo sư, phó
    giáo sư), học vị (đại học, thạc sĩ, tiến sĩ), địa chỉ, điện thoại, số giờ giảng dạy trong tháng, lương
    thỏa thuận (lương cứng ), hệ số lương (đại học: 1, thạc sĩ 1.1 tiến sĩ 1.2, giáo sư thêm 0.2, phó
    giáo sư 0.1) và số giờ quy định chung trong tháng, vượt giờ được tính 50.000đ/1h.
     */
    private Long luongDeal;
    private final int soGioQD = 40;
    private int soGioVuot;

    public GVCoHuu() {
    }

    public GVCoHuu(String maGV, String ten, String email, String hocVi, String hocHam, String diaChi, String dienThoai, int soGioDay, Long luongDeal) {
        super(maGV, ten, email, hocVi, hocHam, diaChi, dienThoai, soGioDay);
        this.luongDeal = luongDeal;
    }

    public Long getLuongDeal() {
        return luongDeal;
    }

    public void setLuongDeal(Long luongDeal) {
        this.luongDeal = luongDeal;
    }

    public int getSoGioQD() {
        return soGioQD;
    }

    public int getSoGioVuot() {
        soGioVuot = getSoGioDay() - soGioQD;
        return soGioVuot;
    }

    @Override
    public double getLuong() {
        return luongDeal * getHeSoLuong() + getSoGioVuot() * 50000;
    }

    @Override
    public String toString() {
        return String.format("%5s %20s %12s %4s %4s %12s %11s %3s %3.1f %12.0f %3s %8s", // 96
                getMaGV(), getTen(), getEmail(), getHocVi(), getHocHam(), getDiaChi(), getDienThoai(), getSoGioDay(), getHeSoLuong(), getLuong(), getSoGioVuot(), getLuongDeal());
    }
}
