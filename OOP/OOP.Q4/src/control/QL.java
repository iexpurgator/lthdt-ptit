package control;

/**
 *
 * @author Hoan
 */
public interface QL {

    /*
    1. Nhập vào thông tin của giảng viên (cơ hữu và thỉnh giảng), có kiểm tra nhập liệu cho mã
    giảng viên
    2. In ra danh sách toàn bộ giảng viên (in vào 1 bảng hoặc in ra 2 bảng), có tính tổng số giảng
    viên ở cuối bảng)
    3. Xóa, sửa giảng viên (nhập vào mã giảng viên)
    4. In ra danh sách giảng viên cơ hữu (có tính tổng)
    5. In ra danh sách giảng viên thỉnh giảng (có tính tổng)
    6. Tìm kiếm giảng viên gần đúng,.....
    7. Sắp xếp danh sách theo họ tên, hệ số lương, theo lương lĩnh....
    8. Tính tổng số tiền lương của toàn bộ giảng viên, tung bình lương....
    9. Tìm giảng viên có tổng lương cao nhất
     */
    public void nhapGVCoHuu();

    public void nhapGVThGiang();

    public void dsGV();

    public void xoa(String maGV);

    public void sua(String maGV);

    public void dsGVch();

    public void dsGVtg();

    public void sapxep();

    public void tinhluong();

    public void timluongcaonhat();

}
