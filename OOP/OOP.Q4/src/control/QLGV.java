package control;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Scanner;
import javafx.stage.Popup;
import model.GV;
import model.GVCoHuu;
import model.GVThGiang;

/**
 *
 * @author Hoan
 */
public class QLGV implements QL {

    private Scanner in;
    private List<GV> list;

    public QLGV() {
        list = new ArrayList<>();
        in = new Scanner(System.in);
        list.add(new GVCoHuu("CH123", "Nguyen Van A", "a@a.com", "ThS", "", "HoaBinh", "0386547618", 53, new Long(10000000)));
        list.add(new GVCoHuu("CH874", "Tran Van A", "a@a.com", "TS", "GS", "Hanoi", "0386547618", 80, new Long(19000000)));
        list.add(new GVThGiang("TG935", "Dinh Van A", "a@a.com", "TS", "PGS", "HungYen", "0386547618", 77, "HaiPhong", "0386547618"));
        list.add(new GVCoHuu("CH143", "Phan Van A", "a@a.com", "TS", "", "BacNinh", "0386547618", 69, new Long(11000000)));
        list.add(new GVThGiang("TG174", "Le Van A", "a@a.com", "TS", "PGS", "HaiPhong", "0386547618", 49, "HaiPhong", "0386547618"));
    }

    private int isAvaliable(String maGV) {
        for (int i = 0; i < list.size(); ++i) {
            if (list.get(i).getMaGV().equalsIgnoreCase(maGV)) {
                return i;
            }
        }
        return -1;
    }

    private GV nhapGV() {
        String maGV;
        do {
            System.out.print("Ma giao vien: ");
            maGV = in.nextLine().toUpperCase();
        } while (!maGV.matches("^[A-Z]{2}[0-9]{3}"));
        String ten;
        System.out.print("Ten: ");
        ten = in.nextLine();
        String email;
        do {
            System.out.print("Email: ");
            email = in.nextLine();
        } while (!email.matches("^[a-zA-Z0-9._]+@[a-zA-Z0-9.-]+$"));
        String hocHam;
        System.out.print("Hoc ham:");
        hocHam = in.nextLine();
        String hocVi;
        System.out.print("Hoc vi: ");
        hocVi = in.nextLine();
        String diaChi;
        System.out.print("Dia chi: ");
        diaChi = in.nextLine();
        String dienThoai;
        do {
            System.out.print("SDT: ");
            dienThoai = in.nextLine();
        } while (!dienThoai.matches("[0-9]{10}"));
        int soGioDay;
        System.out.print("So gio day: ");
        soGioDay = Integer.parseInt(in.nextLine());
        return new GV(maGV, ten, email, hocHam, hocVi, diaChi, dienThoai, soGioDay);
    }

    @Override
    public void nhapGVCoHuu() {
        GV n = nhapGV();
        Long luongDeal;
        System.out.print("Luong thoa thuan: ");
        luongDeal = Long.parseLong(in.nextLine());
        list.add(new GVCoHuu(n.getMaGV(), n.getTen(), n.getEmail(), n.getHocHam(), n.getHocVi(), n.getDiaChi(), n.getDienThoai(), n.getSoGioDay(), luongDeal));
    }

    @Override
    public void nhapGVThGiang() {
        GV n = nhapGV();
        String dcCQ;
        System.out.print("Dia chi: ");
        dcCQ = in.nextLine();
        String sdtCQ;
        do {
            System.out.print("SDT: ");
            sdtCQ = in.nextLine();
        } while (!sdtCQ.matches("[0-9]{10}"));
        list.add(new GVThGiang(n.getMaGV(), n.getTen(), n.getEmail(), n.getHocHam(), n.getHocVi(), n.getDiaChi(), n.getDienThoai(), n.getSoGioDay(), dcCQ, sdtCQ));
    }

    @Override
    public void dsGV() {
//        dsGVch();
//        dsGVtg();
        list.forEach(System.out::println);
    }

    @Override
    public void xoa(String maGV) {
        int i = isAvaliable(maGV);
        if (i != -1) {
            list.remove(i);
        }
    }

    @Override
    public void sua(String maGV) {
        int i = isAvaliable(maGV);
        if (i != -1) {
            list.remove(i);
        }
    }

    @Override
    public void dsGVch() {
        for (GV i : list) {
            if (i instanceof GVCoHuu) {
                System.out.println((GVCoHuu) i);
            }
        }
    }

    @Override
    public void dsGVtg() {
        for (GV i : list) {
            if (i instanceof GVThGiang) {
                System.out.println((GVThGiang) i);
            }
        }
    }

    @Override
    public void sapxep() {
        dsGV();
        System.out.println("");
        Collections.sort(list, new Comparator<GV>() {
            @Override
            public int compare(GV c1, GV c2) {
                int a1 = (int) (c1.getLuong());
                int a2 = (int) (c2.getLuong());
                return a2 - a1;
            }
        });
//        Collections.sort(list, new Comparator<GV>() {
//            @Override
//            public int compare(GV c1, GV c2) {
//                int a1 = (int) (10 * c1.getHeSoLuong());
//                int a2 = (int) (10 * c2.getHeSoLuong());
//                return a1 - a2;
//            }
//        });
//        Collections.sort(list, new Comparator<GV>() {
//            @Override
//            public int compare(GV c1, GV c2) {
//                String s1[] = c1.getTen().split("\\s+");
//                String s2[] = c2.getTen().split("\\s+");
//                String n1 = s1[s1.length -1] + c1.getTen();
//                String n2 = s2[s2.length -1] + c2.getTen();
//                return n1.compareToIgnoreCase(n2);
//            }
//        });
        dsGV();
    }

    @Override
    public void tinhluong() {
        double tongLuong = 0;
        tongLuong = list.stream().map((i) -> i.getLuong()).reduce(tongLuong, (Double accumulator, Double _item) -> {
            return accumulator + _item;
        });
        System.out.println(String.format("Tong luong: %.0f", tongLuong));
    }

    @Override
    public void timluongcaonhat() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
