package oop.q2;

/**
 *
 * @author Hoan
 */
public interface Manager {

    public void showTableVehicle();

    public void sortCompany();

    public void inputCar();

    public void inputMoto();

    public void inputTruck();

    public void seacrhVehicle();

    public void seacrhVehicleInRange();

    public void seacrhAttribute();

    public void cacularting();
}
