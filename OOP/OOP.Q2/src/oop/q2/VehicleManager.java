package oop.q2;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Comparator;
import static oop.q2.OOPQ2.sc;

/**
 *
 * @author Hoan
 */
public class VehicleManager implements Manager{

    private ArrayList<Vehicle> vehicles;

    public VehicleManager() {
        vehicles = new ArrayList<>();
        vehicles.add(new Car("Ford", 2012, new BigInteger("999999"), "Orange", "v8", 4));
        vehicles.add(new Car("Ferari", 2013, new BigInteger("1000999"), "Black", "v10", 2));
        vehicles.add(new Car("Benz", 2011, new BigInteger("1009999"), "Space Grey", "v12", 4));
        vehicles.add(new Truck("Toyota", 2011, new BigInteger("4999"), "Blue", new BigInteger("9000")));
        vehicles.add(new Moto("Honda", 2011, new BigInteger("999"), "White", 43));
    }

    private Vehicle input() {
        String Company;
        do {
            System.out.println("Nhap cong ty sx: ");
            Company = sc.nextLine();
        } while (!Company.isEmpty() && !Company.matches(".*\\d.*") && Company.length() > 2);
        Integer Year;
        System.out.println("Nhap nam sx(so): ");
        Year = Integer.parseInt(sc.nextLine());
        BigInteger Price;
        System.out.println("Nhap gia: ");
        Price = new BigInteger(sc.nextLine());
        String Color;
        System.out.println("Nhap mau: ");
        Color = sc.nextLine();
        return new Vehicle(Company, Year, Price, Color);
    }

    @Override
    public void inputCar() {
        Vehicle in = input();
        String Engine = sc.nextLine();
        int NumOfSeats = Integer.parseInt(sc.nextLine());
        vehicles.add(new Car(in.getCompany(), in.getYear(), in.getPrice(), in.getColor(), Engine, NumOfSeats));
    }

    @Override
    public void inputMoto() {
        Vehicle in = input();
        int Power;
        System.out.println("Nhap trong tai: ");
        Power = Integer.parseInt(sc.nextLine());
        vehicles.add(new Moto(in.getCompany(), in.getYear(), in.getPrice(), in.getColor(), Power));
    }

    @Override
    public void inputTruck() {
        Vehicle in = input();
        BigInteger Tonnage;
        System.out.println("Nhap trong tai (so): ");
        Tonnage = new BigInteger(sc.nextLine());
        vehicles.add(new Truck(in.getCompany(), in.getYear(), in.getPrice(), in.getColor(), Tonnage));
    }

    @Override
    public void showTableVehicle() {
        String path = "";
        for (int i = 0; i < 100; ++i) {
            path += "-";
        }
        System.out.println(path);
        System.out.printf("%15s %6s %12s %20s %15s %5s %10s %8s", "COMPANY", "YEAR", "PRICE", "COLOR", "ENGINE", "SEATS", "TONNAGE", "POWER");
        System.out.println();
        System.out.println(path);
        getVehicles().forEach(System.out::println);
        System.out.println(path);
        System.out.println("TOTAL VEHICLE: " + getVehicles().size() + "\n");
    }

    @Override
    public void sortCompany() {
        vehicles.sort(Comparator.comparing(Vehicle::getCompany));
    }

    public ArrayList<Vehicle> getVehicles() {
        return vehicles;
    }

    @Override
    public void seacrhVehicle() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void seacrhVehicleInRange() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void seacrhAttribute() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void cacularting() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
