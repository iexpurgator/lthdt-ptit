package oop.q2;

import java.math.BigInteger;

/**
 *
 * @author Hoan
 */
public class Moto extends Vehicle {

    private Integer Power;

    public Moto() {
    }

    public Moto(String Company, Integer Year, BigInteger price, String Color, Integer Power) {
        super(Company, Year, price, Color);
        this.Power = Power;
    }

    public Integer getPower() {
        return Power;
    }

    public void setPower(Integer Power) {
        this.Power = Power;
    }
    
    @Override
    public String toString(){
        return String.format("%15s %6d %12s %20s %15s %5s %10s %8d",
                    getCompany(), getYear(), getPrice(), getColor(), "", "", "", getPower());
    }
}
