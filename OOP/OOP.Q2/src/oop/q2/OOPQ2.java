package oop.q2;

import java.util.Scanner;

/**
 *
 * @author Hoan
 */
public class OOPQ2 {

    static Scanner sc = new Scanner(System.in);

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        VehicleManager gara = new VehicleManager();
        int choose;
        do {
            System.out.println("1. Nhap phuong tien");
            System.out.println("2. Danh sach phuong tien");
            System.out.println("3. Tim kiem phuong tien");
            System.out.println("4. Tim kiem phuong tien theo khoang");
            System.out.println("5. Tim 1 so truong");
            System.out.println("6. Sap xep danh sach phuong tien giao thong");
            System.out.println("7. Tinh toan theo tieu chi");
            System.out.println("0. Thoat");
            System.out.print("\nNhap: ");
            choose = Integer.parseInt(sc.nextLine());
            switch (choose) {
                case 1:
                    Nhap(gara);
                    break;
                case 2:
                    gara.showTableVehicle();
                    break;
                case 3:gara.seacrhVehicle();
                    break;
                case 4:
                    gara.seacrhVehicleInRange();
                    break;
                case 5:
                    gara.seacrhAttribute();
                    break;
                case 6:
                    gara.sortCompany();
                    gara.showTableVehicle();
                    break;
                case 7:
                    gara.cacularting();
                    break;
                case 0:
                    System.out.println("Bye!!!");
                    break;
                default:
                    System.err.println("Nhap sai!!!");
                    break;
            }
        } while (choose != 0);

    }

    public static void Nhap(VehicleManager cur) {
        int choose;
        do {
            System.out.println("1. Nhap oto");
            System.out.println("2. Nhap xe tai");
            System.out.println("3. Nhap xe may");
            System.out.println("0. Ve Menu");
            System.out.print("\nNhap: ");
            choose = Integer.parseInt(sc.nextLine());
            switch (choose) {
                case 1:
                    cur.inputCar();
                    break;
                case 2:
                    cur.inputTruck();
                    break;
                case 3:
                    cur.inputMoto();
                    break;
                case 0:
                    System.out.println("\nMenu:");
                    break;
                default:
                    System.out.println("Nhap sai!!!");
                    break;
            }
        } while (choose != 0);
    }
}
