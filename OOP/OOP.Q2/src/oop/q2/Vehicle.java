package oop.q2;

import java.math.BigInteger;

/**
 *
 * @author Hoan
 */
public class Vehicle {

    private String Company;
    private Integer Year;
    private BigInteger Price;
    private String Color;

    public Vehicle() {
    }

    public Vehicle(String Company, Integer Year, BigInteger Price, String Color) {
        this.Company = Company;
        this.Year = Year;
        this.Price = Price;
        this.Color = Color;
    }

    public String getCompany() {
        return Company;
    }

    public void setCompany(String Company) {
        this.Company = Company;
    }

    public Integer getYear() {
        return Year;
    }

    public void setYear(Integer Year) {
        this.Year = Year;
    }

    public BigInteger getPrice() {
        return Price;
    }

    public void setPrice(BigInteger Price) {
        this.Price = Price;
    }

    public String getColor() {
        return Color;
    }

    public void setColor(String Color) {
        this.Color = Color;
    }
}