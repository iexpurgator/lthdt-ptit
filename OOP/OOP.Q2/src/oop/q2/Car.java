package oop.q2;

import java.math.BigInteger;

/**
 *
 * @author Hoan
 */
public class Car extends Vehicle {

    private String Engine;
    private Integer NumOfSeates;

    public Car() {
    }

    public Car(String Company, Integer Year, BigInteger price, String Color, String Engine, Integer NumOfSeates) {
        super(Company, Year, price, Color);
        this.Engine = Engine;
        this.NumOfSeates = NumOfSeates;
    }

    public String getEngine() {
        return Engine;
    }

    public void setEngine(String Engine) {
        this.Engine = Engine;
    }

    public Integer getNumOfSeates() {
        return NumOfSeates;
    }

    public void setNumOfSeates(Integer NumOfSeates) {
        this.NumOfSeates = NumOfSeates;
    }

    @Override
    public String toString() {
        return String.format("%15s %6d %12s %20s %15s %5d",
                getCompany(), getYear(), getPrice(), getColor(), getEngine(), getNumOfSeates());
    }
}
