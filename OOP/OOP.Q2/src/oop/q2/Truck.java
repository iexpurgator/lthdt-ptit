package oop.q2;

import java.math.BigInteger;

/**
 *
 * @author Hoan
 */
public class Truck extends Vehicle {

    private BigInteger Tonnage;

    public Truck() {
    }

    public Truck(String Company, Integer Year, BigInteger price, String Color, BigInteger Tonnage) {
        super(Company, Year, price, Color);
        this.Tonnage = Tonnage;
    }

    public BigInteger getTonnage() {
        return Tonnage;
    }

    public void setTonnage(BigInteger Tonnage) {
        this.Tonnage = Tonnage;
    }
    
    @Override
    public String toString(){
        return String.format("%15s %6d %12s %20s %15s %5s %10s",
                    getCompany(), getYear(), getPrice(), getColor(), "", "", getTonnage());
    }
}
