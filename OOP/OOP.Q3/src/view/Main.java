package view;

import controller.QuanLiTaiLieu;
import java.util.Scanner;

/**
 *
 * @author Hoan
 */
public class Main {

    public static void main(String[] args) {
        int choose;
        Scanner sc = new Scanner(System.in);
        QuanLiTaiLieu thv = new QuanLiTaiLieu();
        do {
            System.out.println("1. Nhap");
            System.out.println("2. In");
            System.out.println("3. Tim gan dung");
            System.out.println("4. Tim theo khoang");
            System.out.println("5. Xoa");
            System.out.println("6. Sua");
            System.out.println("7. Sap xep");
            System.out.println("8. Tinh toan");
            System.out.print("Nhap: ");
            choose = Integer.parseInt(sc.nextLine());
            switch (choose) {
                case 1:
                    thv.nhapBao();
                    break;
                case 2:
                    thv.dsTL();
                    break;
                case 3:
                    break;
                case 4:
                    thv.timtheokhoang();
                    break;
                case 5:
                    thv.xoa();
                    break;
                case 6:
                    thv.sua();
                    break;
                case 7:break;
                case 8:break;
                case 9:break;
                case 10:break;
                case 11:break;
                case 0:
                    System.out.println("bye!!");
                    break;
                default:
                    break;
            }
        } while (choose != 0);
    }
}
