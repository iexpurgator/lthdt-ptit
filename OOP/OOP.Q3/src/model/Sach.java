package model;

/**
 *
 * @author Hoan
 */
public class Sach extends TaiLieu{
    private String TacGia;
    private String TenSach;
    private Integer SoTrang;

    public Sach() {
    }

    public Sach(String MaTL, String NXB, Integer SoBan, String TacGia, String TenSach, Integer SoTrang) {
        super(MaTL, NXB, SoBan);
        this.TacGia = TacGia;
        this.TenSach = TenSach;
        this.SoTrang = SoTrang;
    }

    public String getTacGia() {
        return TacGia;
    }

    public void setTacGia(String TacGia) {
        this.TacGia = TacGia;
    }

    public String getTenSach() {
        return TenSach;
    }

    public void setTenSach(String TenSach) {
        this.TenSach = TenSach;
    }

    public Integer getSoTrang() {
        return SoTrang;
    }

    public void setSoTrang(Integer SoTrang) {
        this.SoTrang = SoTrang;
    }

    @Override
    public String toString() {
        return String.format("%10s %15s %8d %10s %10s %8d",getMaTL(), getNXB(), getSoBan(), TacGia, TenSach, SoTrang);
    }
    
}
