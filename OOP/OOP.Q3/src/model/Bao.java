package model;

/**
 *
 * @author Hoan
 */
public class Bao extends TaiLieu{
    private String NgayPhatHanh;

    public Bao() {
    }

    public Bao(String MaTL, String NXB, Integer SoBan, String NgayPhatHanh) {
        super(MaTL, NXB, SoBan);
        this.NgayPhatHanh = NgayPhatHanh;
    }

    public String getNgayPhatHanh() {
        return NgayPhatHanh;
    }

    public void setNgayPhatHanh(String NgayPhatHanh) {
        this.NgayPhatHanh = NgayPhatHanh;
    }

    @Override
    public String toString() {
        return String.format("%10s %15s %8d %10s",getMaTL(), getNXB(), getSoBan(), NgayPhatHanh);
    }
}
