package model;

/**
 *
 * @author Hoan
 */
public class TaiLieu {
    // Mã tài liệu (không trùng), Tên nhà xuất bản, Số bản phát hành.
    private String MaTL;
    private String NXB;
    private Integer SoBan;

    public TaiLieu() {
    }

    public TaiLieu(String MaTL, String NXB, Integer SoBan) {
        this.MaTL = MaTL;
        this.NXB = NXB;
        this.SoBan = SoBan;
    }

    public String getMaTL() {
        return MaTL;
    }

    public void setMaTL(String MaTL) {
        this.MaTL = MaTL;
    }

    public String getNXB() {
        return NXB;
    }

    public void setNXB(String NXB) {
        this.NXB = NXB;
    }

    public Integer getSoBan() {
        return SoBan;
    }

    public void setSoBan(Integer SoBan) {
        this.SoBan = SoBan;
    }
}
