package model;

/**
 *
 * @author Hoan
 */
public class TapChi extends TaiLieu{
    private Integer SoPhatHanh;
    private String ThangPhatHanh;

    public TapChi() {
    }

    public TapChi(String MaTL, String NXB, Integer SoBan, Integer SoPhatHanh, String ThangPhatHanh) {
        super(MaTL, NXB, SoBan);
        this.SoPhatHanh = SoPhatHanh;
        this.ThangPhatHanh = ThangPhatHanh;
    }

    public Integer getSoPhatHanh() {
        return SoPhatHanh;
    }

    public void setSoPhatHanh(Integer SoPhatHanh) {
        this.SoPhatHanh = SoPhatHanh;
    }

    public String getThangPhatHanh() {
        return ThangPhatHanh;
    }

    public void setThangPhatHanh(String ThangPhatHanh) {
        this.ThangPhatHanh = ThangPhatHanh;
    }

    @Override
    public String toString() {
        return String.format("%10s %15s %8d %10s %10s",getMaTL(), getNXB(), getSoBan(), SoPhatHanh, ThangPhatHanh);
    }
    
    
}
