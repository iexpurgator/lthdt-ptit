package controller;

/**
 *
 * @author Hoan
 */
public interface ChucNang {
    public void nhapSach();
    public void nhapTapChi();
    public void nhapBao();
    public void dsTL();
    public void timtheokhoang();
    public void xoa();
    public void sua();
    public void tinh_toan();
}
