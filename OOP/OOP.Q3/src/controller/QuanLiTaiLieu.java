package controller;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;
import model.Bao;
import model.Sach;
import model.TaiLieu;
import model.TapChi;

/**
 *
 * @author Hoan
 */
public class QuanLiTaiLieu implements ChucNang {

    private ArrayList<TaiLieu> ds;
    private Scanner sc;

    public QuanLiTaiLieu() {
        ds = new ArrayList<>();
        sc = new Scanner(System.in);
        ds.add(new Sach("SAC123", "iss", 123, "TacGia", "Ten Sach", 213));
        ds.add(new TapChi("TAC234", "isa", 234, 69, "12/2012"));
        ds.add(new Bao("BAO345", "usq", 345, "12/12/1212"));
    }

    private int timTL(String MaTL) {
        for (int i = 0; i < ds.size(); i++) {
            if (MaTL.equals(ds.get(i).getMaTL())) {
                return i;
            }
        }
        return -1;
    }

    private TaiLieu nhapTaiLieu() {
        String MaTL;
        String re = "^[A-Z]{3}\\d{3}"; // 3 chu o dau vao 3 so o cuoi
        do {
            System.out.print("Nhap ma tai lieu: ");
            MaTL = sc.nextLine().toUpperCase();
        } while (!MaTL.matches(re) && timTL(MaTL) == -1);
        String NXB;
        System.out.print("Nhap nxb: ");
        NXB = sc.nextLine();
        Integer SoBan;
        System.out.print("Nhap so ban (so): ");
        SoBan = Integer.parseInt(sc.nextLine());
        return new TaiLieu(MaTL, NXB, SoBan);
    }

    @Override
    public void nhapSach() {
        TaiLieu in = nhapTaiLieu();
        String TacGia;
        System.out.print("Nhap tac gia: ");
        do {
            TacGia = sc.nextLine().toLowerCase();
            String result = "";
            for (String w : TacGia.split(" +")) {
                result += (Character.toUpperCase(w.charAt(0)) + w.substring(1) + " ");
            }
            TacGia = result.trim().replaceAll(" +", " ");
        } while (!TacGia.matches("[a-zA-Z ]+"));
        String TenSach;
        System.out.print("Nhap ten sach: ");
        TenSach = sc.nextLine();
        Integer SoTrang;
        System.out.print("Nhap so trang(so): ");
        SoTrang = Integer.parseInt(sc.nextLine());
        ds.add(new Sach(in.getMaTL(), in.getNXB(), in.getSoBan(), TacGia, TenSach, SoTrang));
    }

    @Override
    public void nhapTapChi() {
        TaiLieu in = nhapTaiLieu();
        Integer SoPhatHanh;
        System.out.print("Nhap so phat hanh: ");
        SoPhatHanh = Integer.parseInt(sc.nextLine());
        String ThangPhatHanh;
        System.out.print("Nhap thang phat hanh: ");
        ThangPhatHanh = sc.nextLine();
        ds.add(new TapChi(in.getMaTL(), in.getNXB(), in.getSoBan(), SoPhatHanh, ThangPhatHanh));
    }

    @Override
    public void nhapBao() {
        TaiLieu in = nhapTaiLieu();
        String NgayPhatHanh;
        System.out.print("Nhap ngay phat hanh: ");
        NgayPhatHanh = sc.nextLine();
        ds.add(new Bao(in.getMaTL(), in.getNXB(), in.getSoBan(), NgayPhatHanh));
    }

    @Override
    public void dsTL() {
        System.out.println(String.format("%66s", "").replace(' ', '='));
        System.out.println(String.format("%10s %15s %8s %-28s", "MATL", "NXB", "SoBan", "Thong tin khac"));
        System.out.println(String.join("", Collections.nCopies(66, "-")));
        ds.forEach(System.out::println);
        System.out.println(String.join("", Collections.nCopies(66, "-")));
        System.out.println("Tong so tai lieu: " + ds.size());
        System.out.println(String.format("%66s\n", "").replace(' ', '='));
    }

    private void dsSach() {
        int count = 0;
        System.out.println(String.format("%66s", "").replace(' ', '='));
        System.out.println(String.format("%10s %15s %8s %10s %10s %8s", "MATL", "NXB", "SoBan", "TacGia", "TenSach", "SoTrang"));
        System.out.println(String.format("%66s", "").replace(' ', '-'));
        for (TaiLieu i : ds) {
            if (i instanceof Sach) {
                System.out.println(i);
                count++;
            }
        }
        System.out.println(String.format("%66s", "").replace(' ', '-'));
        System.out.println("Tong so SACH: " + count);
        System.out.println(String.format("%66s\n", "").replace(' ', '='));
    }

    private void dsTapChi() {
        int count = 0;
        System.out.println(String.format("%57s", "").replace(' ', '='));
        System.out.println(String.format("%10s %15s %8s %10s %10s", "MATL", "NXB", "SoBan", "SoPH", "ThangPH"));
        System.out.println(String.format("%57s", "").replace(' ', '-'));
        for (TaiLieu i : ds) {
            if (i instanceof TapChi) {
                System.out.println(i);
                count++;
            }
        }
        System.out.println(String.format("%57s", "").replace(' ', '-'));
        System.out.println("Tong so TAP CHI: " + count);
        System.out.println(String.format("%57s\n", "").replace(' ', '='));
    }

    private void dsBao() {
        int count = 0;
        System.out.println(String.format("%46s", "").replace(' ', '='));
        System.out.println(String.format("%10s %15s %8s %10s", "MATL", "NXB", "SoBan", "NgayPH"));
        System.out.println(String.format("%46s", "").replace(' ', '-'));
        for (TaiLieu i : ds) {
            if (i instanceof Bao) {
                System.out.println(i);
                count++;
            }
        }
        System.out.println(String.format("%46s", "").replace(' ', '-'));
        System.out.println("Tong so BAO: " + count);
        System.out.println(String.format("%46s\n", "").replace(' ', '='));
    }

    @Override
    public void timtheokhoang() {
        System.out.println("Nhap khoang tim kiem\nTu: ");
        Integer low = Integer.parseInt(sc.nextLine());
        System.out.println("Den: ");
        Integer high = Integer.parseInt(sc.nextLine());
        System.out.println(String.format("%66s", "").replace(' ', '='));
        System.out.println(String.format("%10s %15s %8s %-28s", "MATL", "NXB", "SoBan", "Thong tin khac"));
        System.out.println(String.join("", Collections.nCopies(66, "-")));
        for(TaiLieu i : ds){
            if (low <= i.getSoBan() && i.getSoBan() <= high){
                System.out.println(i);
            }
        }
        System.out.println(String.join("", Collections.nCopies(66, "-")));
        System.out.println("Tong so tai lieu: " + ds.size());
        System.out.println(String.format("%66s\n", "").replace(' ', '='));
    }

    @Override
    public void xoa() {
        String MaTL;
        System.out.print("Nhap ma tai lieu can xoa: ");
        MaTL = sc.nextLine().toUpperCase();
        int idx = timTL(MaTL);
        if (idx != -1) {
            System.err.println("Khong tim thay de xoa!!!");
        } else {
            ds.remove(idx);
            System.out.println("Xoa thanh cong");
        }
    }

    @Override
    public void sua() {
        String MaTL;
        System.out.print("Nhap ma tai lieu can xoa: ");
        MaTL = sc.nextLine().toUpperCase();
        int idx = timTL(MaTL);
        if (idx != -1) {
            System.err.println("Khong tim thay de sua!!!");
        } else {
            TaiLieu tl;
            tl = ds.get(idx);
            String NXB;
            System.out.println("Nhap nxb:");
            NXB = sc.nextLine();
            tl.setNXB(NXB);
            Integer SoBan;
            System.out.println("Nhap so ban (so):");
            SoBan = Integer.parseInt(sc.nextLine());
            tl.setSoBan(SoBan);
            System.out.println("Sua thanh cong");
        }
    }

    @Override
    public void tinh_toan() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
